﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Insights.Models.Adwords;
using Insights.DAL;
using System.Data.Entity;
using System.Configuration;
using Insights.Repository;

namespace Insights.Controllers
{

    public class HomeController : Controller
    {
        readonly IRepository _repository;
        public HomeController(IRepository userRepository)
        {
            this._repository = userRepository;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Accounts()
        {            
            return View();
        }
        public ActionResult Campaigns(Int64? id)
        {
            ViewBag.id = id == null ? 0 : id;
            return View();
        }
        public ActionResult Adgroups(Int64? id)
        {
            ViewBag.id = id == null ?0: id;
            return View();
        }
        public ActionResult Keywords(Int64? id)
        {
            ViewBag.id = id == null ? 0 : id;
            return View();
        }
      
        public ActionResult Adgroups_Read(Int64 id,[DataSourceRequest]DataSourceRequest request)
        {
            return Json(_repository.GetAdgroups(id).ToDataSourceResult(request));
        }
        public ActionResult Keywords_Read(Int64 id, [DataSourceRequest]DataSourceRequest request)
        {
            return Json(_repository.GetKeywords(id, request).ToDataSourceResult(request));
        }
        public ActionResult Accounts_Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(_repository.GetAccount().ToDataSourceResult(request));
        }
        public ActionResult Campaigns_Read(Int64 id, [DataSourceRequest]DataSourceRequest request)
        {
            return Json(_repository.GetCampaigns(id).ToDataSourceResult(request));
        }
    }
}