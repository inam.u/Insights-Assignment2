﻿using Insights.Models.Adwords;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Insights.DAL
{
    public class DataAccess : IDisposable
    {
        private readonly string connectionString;
       
        public DataAccess()
        {
            this.connectionString = ConfigurationManager.ConnectionStrings["InsightsString"].ToString();
        }
        public List<KeywordFact> GetAccount()
        {
            List<KeywordFact> dataList = new List<KeywordFact>();
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
            

            using (SqlCommand cmd = Connection.CreateCommand())
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.CommandText = "SP_Account";
                Connection.Open();
                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    if (dataReader.HasRows)
                    {
                      
                        while (dataReader.Read())
                        {
                            dataList.Add(new KeywordFact
                            {
                                AccountId = dataReader["AccountId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AccountId")),
                                AccountName = dataReader["AccountName"] is DBNull ? null : dataReader["AccountName"].ToString(),
                                Clicks = dataReader["Clicks"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Clicks")),
                                Cost = dataReader["Cost"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Cost")),
                                Revenue = dataReader["Revenue"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Revenue")),
                            });
                        }
                    }
                    dataReader.Close();
                }
                Connection.Close();
            }
            }



            //accountList.Add(new KeywordFact
            //{
            //    AccountId = 1,
            //    AccountName = "inam ul haq"
            //});
            //accountList.Add(new Account
            //{
            //    AccountId = 2,
            //    AccountName = "inam "
            //});
            return dataList;
        }
        public List<KeywordFact> GetCampaigns(Int64 accountID)
        {
            List<KeywordFact> dataList = new List<KeywordFact>();
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {


                using (SqlCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Campaign";
                    cmd.Parameters.AddWithValue("@AccountId", accountID);
                    Connection.Open();
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {

                            while (dataReader.Read())
                            {
                                dataList.Add(new KeywordFact
                                {
                                    AccountId = dataReader["AccountId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AccountId")),
                                    AccountName = dataReader["AccountName"] is DBNull ? null : dataReader["AccountName"].ToString(),
                                    CampaignId = dataReader["CampaignId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("CampaignId")),
                                    CampaignName = dataReader["CampaignName"] is DBNull ? null : dataReader["CampaignName"].ToString(),
                                    Clicks = dataReader["Clicks"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Clicks")),
                                    Cost = dataReader["Cost"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Cost")),
                                    Revenue = dataReader["Revenue"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Revenue")),
                                });
                            }
                        }
                        dataReader.Close();
                    }
                    Connection.Close();
                }
            }

            return dataList;
        }
        public List<KeywordFact> GetAdgroups(Int64 campaignId)
        {
            List<KeywordFact> dataList = new List<KeywordFact>();
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {


                using (SqlCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Adgroup";
                    cmd.Parameters.AddWithValue("@CampaignId", campaignId);
                    Connection.Open();
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {

                            while (dataReader.Read())
                            {
                                dataList.Add(new KeywordFact
                                {
                                    AccountId = dataReader["AccountId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AccountId")),
                                    AccountName = dataReader["AccountName"] is DBNull ? null : dataReader["AccountName"].ToString(),
                                    CampaignId = dataReader["CampaignId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("CampaignId")),
                                    CampaignName = dataReader["CampaignName"] is DBNull ? null : dataReader["CampaignName"].ToString(),
                                    AdgroupId = dataReader["AdgroupId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AdgroupId")),
                                    AdgroupName = dataReader["AdgroupName"] is DBNull ? null : dataReader["AdgroupName"].ToString(),
                                    Clicks = dataReader["Clicks"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Clicks")),
                                    Cost = dataReader["Cost"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Cost")),
                                    Revenue = dataReader["Revenue"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Revenue")),
                                });
                            }
                        }
                        dataReader.Close();
                    }
                    Connection.Close();
                }
            }

            return dataList;
        }
        public List<KeywordFact> GetKeywords(Int64 adgroupId)
        {
            List<KeywordFact> dataList = new List<KeywordFact>();
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Keyword";
                    cmd.Parameters.AddWithValue("@AdgroupId", adgroupId);
                    Connection.Open();
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {

                            while (dataReader.Read())
                            {
                                dataList.Add(new KeywordFact
                                {
                                    AccountId = dataReader["AccountId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AccountId")),
                                    AccountName = dataReader["AccountName"] is DBNull ? null : dataReader["AccountName"].ToString(),
                                    CampaignId = dataReader["CampaignId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("CampaignId")),
                                    CampaignName = dataReader["CampaignName"] is DBNull ? null : dataReader["CampaignName"].ToString(),
                                    AdgroupId = dataReader["AdgroupId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AdgroupId")),
                                    AdgroupName = dataReader["AdgroupName"] is DBNull ? null : dataReader["AdgroupName"].ToString(),
                                    KeywordId = dataReader["KeywordId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("KeywordId")),
                                    Keyword = dataReader["Keyword"] is DBNull ? null : dataReader["Keyword"].ToString(),
                                    Clicks = dataReader["Clicks"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Clicks")),
                                    Cost = dataReader["Cost"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Cost")),
                                    Revenue = dataReader["Revenue"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Revenue")),
                                });
                            }
                        }
                        dataReader.Close();
                    }
                    Connection.Close();
                }
            }

            return dataList;
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~DataAccess() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}