﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insights.Models.Adwords
{
    public class Account
    {
        public Int64 AccountId { get; set; }
        public String AccountName { get; set; }
    }
}