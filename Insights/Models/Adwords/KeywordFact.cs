﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insights.Models.Adwords
{
    public class KeywordFact
    {
        public Int64 AccountId { get; set; }
        public String AccountName { get; set; }
        public Int64 CampaignId { get; set; }
        public String CampaignName { get; set; }
        public Int64 AdgroupId { get; set; }
        public String AdgroupName { get; set; }
        public Int64 KeywordId { get; set; }
        public String Keyword { get; set; }
        public DateTime Day { get; set; }
        public Int32 Clicks { get; set; }
        public Int32 Cost { get; set; }
        public Int32 Revenue { get; set; }
    }
}