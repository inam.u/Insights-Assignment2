﻿using Insights.Models.Adwords;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insights.Repository
{
    public interface IRepository
    {
        Int64 GetAll();
        List<KeywordFact> GetAccount();
        List<KeywordFact> GetCampaigns(Int64 accountID);
        List<KeywordFact> GetAdgroups(Int64 campaignId);
        List<KeywordFact> GetKeywords(Int64 adgroupId, DataSourceRequest src);
    }
}
