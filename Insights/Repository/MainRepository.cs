﻿using Insights.Models;
using Insights.Models.Adwords;
using Kendo.Mvc;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace Insights.Repository
{
    public class MainRepository : IRepository
    {
        private readonly string connectionString;
        //private readonly ApplicationDbContext ApplicationDbContext;
        public MainRepository(ApplicationDbContext ApplicationDbContext)
        {
            //this.ApplicationDbContext = ApplicationDbContext;
            this.connectionString = ApplicationDbContext.Database.Connection.ConnectionString;
        }
        public Int64 GetAll()
        {
            return 1;
        }
        public List<KeywordFact> GetAccount()
        {
            List<KeywordFact> dataList = new List<KeywordFact>();
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {


                using (SqlCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Account";
                    Connection.Open();
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {

                            while (dataReader.Read())
                            {
                                dataList.Add(new KeywordFact
                                {
                                    AccountId = dataReader["AccountId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AccountId")),
                                    AccountName = dataReader["AccountName"] is DBNull ? null : dataReader["AccountName"].ToString(),
                                    Clicks = dataReader["Clicks"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Clicks")),
                                    Cost = dataReader["Cost"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Cost")),
                                    Revenue = dataReader["Revenue"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Revenue")),
                                });
                            }
                        }
                        dataReader.Close();
                    }
                    Connection.Close();
                }
            }



            //accountList.Add(new KeywordFact
            //{
            //    AccountId = 1,
            //    AccountName = "inam ul haq"
            //});
            //accountList.Add(new Account
            //{
            //    AccountId = 2,
            //    AccountName = "inam "
            //});
            return dataList;
        }
        public List<KeywordFact> GetCampaigns(Int64 accountID)
        {
            List<KeywordFact> dataList = new List<KeywordFact>();
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Campaign";
                    cmd.Parameters.AddWithValue("@AccountId", accountID);
                    Connection.Open();
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {

                            while (dataReader.Read())
                            {
                                dataList.Add(new KeywordFact
                                {
                                    AccountId = dataReader["AccountId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AccountId")),
                                    AccountName = dataReader["AccountName"] is DBNull ? null : dataReader["AccountName"].ToString(),
                                    CampaignId = dataReader["CampaignId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("CampaignId")),
                                    CampaignName = dataReader["CampaignName"] is DBNull ? null : dataReader["CampaignName"].ToString(),
                                    Clicks = dataReader["Clicks"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Clicks")),
                                    Cost = dataReader["Cost"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Cost")),
                                    Revenue = dataReader["Revenue"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Revenue")),
                                });
                            }
                        }
                        dataReader.Close();
                    }
                    Connection.Close();
                }
            }

            return dataList;
        }
        public List<KeywordFact> GetAdgroups(Int64 campaignId)
        {
            List<KeywordFact> dataList = new List<KeywordFact>();
            using (SqlConnection Connection = new SqlConnection(connectionString))
            {


                using (SqlCommand cmd = Connection.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandText = "SP_Adgroup";
                    cmd.Parameters.AddWithValue("@CampaignId", campaignId);
                    Connection.Open();
                    using (SqlDataReader dataReader = cmd.ExecuteReader())
                    {
                        if (dataReader.HasRows)
                        {

                            while (dataReader.Read())
                            {
                                dataList.Add(new KeywordFact
                                {
                                    AccountId = dataReader["AccountId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AccountId")),
                                    AccountName = dataReader["AccountName"] is DBNull ? null : dataReader["AccountName"].ToString(),
                                    CampaignId = dataReader["CampaignId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("CampaignId")),
                                    CampaignName = dataReader["CampaignName"] is DBNull ? null : dataReader["CampaignName"].ToString(),
                                    AdgroupId = dataReader["AdgroupId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AdgroupId")),
                                    AdgroupName = dataReader["AdgroupName"] is DBNull ? null : dataReader["AdgroupName"].ToString(),
                                    Clicks = dataReader["Clicks"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Clicks")),
                                    Cost = dataReader["Cost"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Cost")),
                                    Revenue = dataReader["Revenue"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Revenue")),
                                });
                            }
                        }
                        dataReader.Close();
                    }
                    Connection.Close();
                }
            }

            return dataList;
        }
        public List<KeywordFact> GetKeywords(Int64 adgroupId, DataSourceRequest src)
        {
            List<KeywordFact> dataList = new List<KeywordFact>();
            try
            {
                using (SqlConnection Connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = Connection.CreateCommand())
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "SP_Keyword";
                        cmd.Parameters.AddWithValue("@AdgroupId", adgroupId);
                        cmd.Parameters.AddWithValue("@Param", src.GetParms());
                        cmd.Parameters.AddWithValue("@Page", src.Page);
                        cmd.Parameters.AddWithValue("@PageSize", src.PageSize);
                        Connection.Open();
                        using (SqlDataReader dataReader = cmd.ExecuteReader())
                        {
                            if (dataReader.HasRows)
                            {
                                while (dataReader.Read())
                                {
                                    dataList.Add(new KeywordFact
                                    {
                                        AccountId = dataReader["AccountId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AccountId")),
                                        AccountName = dataReader["AccountName"] is DBNull ? null : dataReader["AccountName"].ToString(),
                                        CampaignId = dataReader["CampaignId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("CampaignId")),
                                        CampaignName = dataReader["CampaignName"] is DBNull ? null : dataReader["CampaignName"].ToString(),
                                        AdgroupId = dataReader["AdgroupId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("AdgroupId")),
                                        AdgroupName = dataReader["AdgroupName"] is DBNull ? null : dataReader["AdgroupName"].ToString(),
                                        KeywordId = dataReader["KeywordId"] is DBNull ? 0 : dataReader.GetInt64(dataReader.GetOrdinal("KeywordId")),
                                        Keyword = dataReader["Keyword"] is DBNull ? null : dataReader["Keyword"].ToString(),
                                        Clicks = dataReader["Clicks"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Clicks")),
                                        Cost = dataReader["Cost"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Cost")),
                                        Revenue = dataReader["Revenue"] is DBNull ? 0 : dataReader.GetInt32(dataReader.GetOrdinal("Revenue")),
                                    });
                                }
                            }
                            dataReader.Close();
                        }
                        Connection.Close();
                    }
                }
                return dataList;
            }
            catch (Exception ex)
            {

                return dataList;
            }

        }


    }
    public static class MyExtensions
    {
        public static DataTable GetParms(this DataSourceRequest src)
        {
            int i = 1;
            //XmlDocument xml = new XmlDocument();
            //XmlElement root = xml.CreateElement("parms");
            //xml.AppendChild(root);
            //XmlElement filterParm = xml.CreateElement("filter");
            //root.AppendChild(filterParm);

            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(Int32));
            dt.Columns.Add("Member", typeof(String));
            dt.Columns.Add("Operator", typeof(String));
            dt.Columns.Add("Value", typeof(String));
            dt.Columns.Add("DataType", typeof(String));
            dt.Columns.Add("Opr", typeof(String));
            foreach (IFilterDescriptor filter in src.Filters)
            {
                if (filter.GetType() == typeof(FilterDescriptor))
                {
                    AppendDataTable((FilterDescriptor)filter, ref dt, "");
                }
                else if (filter.GetType() == typeof(CompositeFilterDescriptor))
                {
                    AppendDataTable((CompositeFilterDescriptor)filter, ref dt, "");
                }
                i++;
            }
            foreach (SortDescriptor filter in src.Sorts)
            {
                dt.Rows.Add(new Object[]
            {
               dt.Rows.Count+1,
               "",
               filter.Member,
                filter.SortDirection.ToString() == "Ascending"? "Asc":"Desc",
               "Order",
                ""
            });
            }
            return dt;

        }
        public static void AppendElement(FilterDescriptor descriptor, ref XmlDocument xml, ref XmlElement filterParm)
        {
            XmlElement whereFilter = xml.CreateElement("Where");
            filterParm.AppendChild(whereFilter);
            XmlElement Member = xml.CreateElement("Member");
            Member.InnerText = descriptor.Member.ToString();
            whereFilter.AppendChild(Member);
            XmlElement Operator = xml.CreateElement("Operator");
            Operator.InnerText = descriptor.Operator.ToString();
            whereFilter.AppendChild(Operator);
            XmlElement Value = xml.CreateElement("Value");
            Value.InnerText = descriptor.Value.ToString();
            XmlElement DataType = xml.CreateElement("DataType");
            DataType.InnerText = typeof(KeywordFact).GetProperty(descriptor.Member.ToString()).PropertyType.ToString();
            whereFilter.AppendChild(Value);
            whereFilter.AppendChild(DataType);


        }
        public static void AppendCompositElement(CompositeFilterDescriptor descriptor, ref XmlDocument xml, ref XmlElement filterParm)
        {
            foreach (var item in descriptor.FilterDescriptors)
            {
                if (item.GetType() == typeof(FilterDescriptor))
                {
                    AppendElement((FilterDescriptor)item, ref xml, ref filterParm);
                }
                else if (item.GetType() == typeof(CompositeFilterDescriptor))
                {
                    AppendCompositElement((CompositeFilterDescriptor)item, ref xml, ref filterParm);
                }

            }

        }

        public static void AppendDataTable(FilterDescriptor descriptor, ref DataTable dt, String Opr)
        {
            int i = dt.Rows.Count + 1;
            dt.Rows.Add(new Object[]
            {
               i,
                descriptor.Member.ToString(),
                descriptor.Operator.ToString().ParseOperator(),
                descriptor.Value.ToString().ParseValue(typeof(KeywordFact).GetProperty(descriptor.Member.ToString()).PropertyType),
                typeof(KeywordFact).GetProperty(descriptor.Member.ToString()).PropertyType.ToString(),
                Opr
            });
        }
        public static void AppendDataTable(CompositeFilterDescriptor descriptor, ref DataTable dt, String Opr)
        {
            foreach (var item in descriptor.FilterDescriptors)
            {

                if (item.GetType() == typeof(FilterDescriptor))
                {
                    AppendDataTable((FilterDescriptor)item, ref dt, Opr);
                }
                else if (item.GetType() == typeof(CompositeFilterDescriptor))
                {
                    var aa = (CompositeFilterDescriptor)item;

                    AppendDataTable((CompositeFilterDescriptor)item, ref dt, aa.LogicalOperator.ToString());
                }

            }
        }

        public static String ParseOperator(this String opr)
        {
            switch (opr)
            {
                case "IsEqualTo":
                    return "=";
                case "IsNotEqualTo":
                    return "<>";
                case "IsGreaterThanOrEqualTo":
                    return ">=";
                case "IsLessThanOrEqualTo":
                    return "<=";
                case "IsGreaterThan":
                    return ">";
                case "IsLessThan":
                    return "<";
                case "IsNotNull":
                    return "is Null";
                case "IsNull":
                    return "is Not Null";
                default:
                    return opr;
            }
        }
        public static String ParseValue(this String opr, Type prop)
        {
            if (prop == typeof(System.String))
            {
                return "'" + opr + "'";
            }
            return opr;
        }
    }
}












//XmlElement Member = xml.CreateElement("Member");
//Member.InnerText = descriptor.Member.ToString();
//filterParm.AppendChild(Member);
//XmlElement Operator = xml.CreateElement("Operator");
//Operator.InnerText = descriptor.Operator.ToString();
//filterParm.AppendChild(Operator);
//XmlElement Value = xml.CreateElement("Value");
//Value.InnerText = descriptor.Value.ToString();
//filterParm.AppendChild(Value);
//if (src.Filters.Count != i)
//{
//    XmlElement condition = xml.CreateElement("Condition");
//    condition.InnerText = "or";
//    filterParm.AppendChild(condition);
//}